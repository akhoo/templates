# Templates

Just some generic templates for use with different things. Uses `LaTeX` and `rmarkdown`.

Render with appropriate

-  `Rscript -e 'rmarkdown::render("document.rmd")'`
-  `pdflatex`
-  `xelatex` for beamer if using fira-sans as the typeface

Merge pdf documents together using

-  `pdfunite` from the `poppler-utils` package

Enter chinese text using:

```
\documentclass{article}
\usepackage{xeCJK}
\setCJKmainfont{Source Han Sans Light}  % check fc-list to see what you've installed
\begin{document}
白居易 hello
\end{document}
```

# Documents

The referencing styles come from [Zotero Style Repository](https://www.zotero.org/styles).


# Presentation

Uses beamer with the colour theme owl. Remove snowy to make it dark theme. Remove the ``\usecolortheme{owl}`` to use default theme, not recommended for `hannover`, but `metropolis` is ok.

-  `metropolis` is better for less text on slides and probably shorter presentations due to lack of ToC bar. Furthermore if rendering, require fira sans typeface, render to `.tex` document and then `xelatex` that `.tex` file.
-  `hannover` has the ToC bar so it seems to be better for longer presentations. Use `\section` and `\subsection` liberally to make them appear on the side - however note that they do not automatically generate a frame for it after it is used.

# Titlepages
