---
title: "STAT1201 Workshop 9: Regression"
author: "Aaron Khoo"
date: "`r format(Sys.Date(), '%d %B %Y')`"
header-includes:
  -  \usetheme{metropolis}
  -  \usecolortheme[snowy]{owl}      % remove snowy if you want dark theme
  -  \resetcounteronoverlays{saveenumi}
output:
  beamer_presentation:
    keep_tex: true
---

# Summary

-  Tutor review
-  Block design
-  Regression
-  Exam


# Linear regression

Statistical method for constructing a relationship between a *continuous* explanatory and a *continuous* response variable.

You are probably familiar with the equation for a straight line $y=mx+c$ where: $y$ = y-value, $m$ = gradient, $x$ = x-value, $c$ = y-intercept.

Statistics uses different notation (reasons seen later) but the idea is the same.

$$y=b_{0}+b_{1}x$$

-  $b_{0}$ = y-intercept
-  $b_{1}$ = gradient

# Example: Mass vs Height

```{r setup, include = FALSE}
survey <- readr::read_csv("9survey.csv")
library(lattice)
knitr::opts_chunk$set(echo = TRUE, size = "footnotesize")
```

```{r}
xyplot(Mass ~ Height, type = c("p"), data=survey)
```


# Example: Mass vs Height AND Sex (cont.)

```{r dafsds, echo=FALSE}
knitr::kable(summary(lm(Mass ~ Height*Sex, data = survey))$coefficients)
```

\begin{align*}
y&=b_{0}+b_{1}x+b_{2}x_{1}+b_{3}xx_{1}\\
&=-52.13+0.69x+-35.72x_{1}+0.21xx_{1}\\
Mass&=-52.13+0.69Height+-35.72Sex+0.21HeightSex
\end{align*}
